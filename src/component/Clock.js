import { Component } from "react";

class Clock extends Component{
    constructor(props) {
        super(props);

        this.state = {
            currentTime: new Date()
        };
    }

    componentDidMount() {
        setInterval(() => {
          this.setState({ currentTime: new Date() });
        }, 1000);
      }
    
    handleChangeColor = () => {
        let {currentTime} = this.state 
        let changeColor =  currentTime.getSeconds() % 2 === 0 ? 'red' : 'blue';
        this.setState({
            color: changeColor
        })
        console.log(this.state.currentTime.getSeconds())
        console.log(this.state.color)
    }

    render(){
        let {currentTime, color} = this.state
        return(
            <div>
                <h1 style={{color: color}}>Hello World</h1>
                <h2>it is {currentTime.toLocaleTimeString()}.</h2>
                <button onClick={this.handleChangeColor}>Change Color</button>
            </div>
        )
    }
}

export default Clock;